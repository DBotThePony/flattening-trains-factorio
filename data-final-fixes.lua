
local indexing = {
	['k'] = math.pow(10, 3),
	['K'] = math.pow(10, 3),
	['M'] = math.pow(10, 6),
	['G'] = math.pow(10, 9),
	['T'] = math.pow(10, 12),
	['P'] = math.pow(10, 15),
	['E'] = math.pow(10, 18),
	['Z'] = math.pow(10, 21),
	['Y'] = math.pow(10, 24),
}

local function convert(units)
	local power = tonumber(units)
	local watts = false

	if not power then
		power = tonumber(units:sub(1, -2))
		watts = true
	end

	if not power then
		power = tonumber(units:sub(1, -3)) * (indexing[units:sub(-2, -2)] or 1)
	end

	return power, watts
end

local cargo_ships = {
	['boat'] = true,
	['boat_engine'] = true,
	['cargo_ship'] = true,
	['oil_tanker'] = true,
	['cargo_ship_engine'] = true,
}

local function slot_mult(value)
	if value == 0 then return 0 end

	return math.max(1, math.ceil(value * settings.startup['flattening-trains-fuel-slot-amount-multiplier'].value - 0.5) + settings.startup['flattening-trains-fuel-slot-amount-flat'].value)
end

for index, value in pairs(data.raw.locomotive) do
	-- пропускаем прототипы мода cargo ships
	if cargo_ships[index] then goto CONTINUE end

	value.weight = value.weight * settings.startup['flattening-trains-weight-multiplier'].value
	local power = convert(value.max_power)
	value.max_power = (power * settings.startup['flattening-trains-power-multiplier'].value) .. 'W'

	if value.braking_force then
		local force, force_w = convert(value.braking_force)

		if force_w then
			value.braking_force = (force * settings.startup['flattening-trains-braking-force'].value) .. 'W'
		else
			value.braking_force = force * settings.startup['flattening-trains-braking-force'].value
		end
	elseif value.braking_power then
		local force, force_w = convert(value.braking_power)

		if force_w then
			value.braking_power = (force * settings.startup['flattening-trains-braking-force'].value) .. 'W'
		else
			value.braking_power = force * settings.startup['flattening-trains-braking-force'].value
		end
	-- don't throw error to allow game UI to display correct error message when assembling prototypes
	--else
	--  error('Locomotive ' .. index .. ' both has no braking_force and no braking_power. This is a bug in other mod!')
	end

	value.max_health = value.max_health * settings.startup['flattening-trains-loco-health'].value
	value.max_speed = value.max_speed * settings.startup['flattening-trains-max-speed'].value

	if value.burner then
		if type(value.burner.effectivity) == 'number' then
			value.burner.effectivity = value.burner.effectivity * settings.startup['flattening-trains-power-efficiency-multiplier'].value
		end

		if type(value.burner.fuel_inventory_size) == 'number' then
			value.burner.fuel_inventory_size = slot_mult(value.burner.fuel_inventory_size)
		end

		if type(value.burner.burnt_inventory_size) == 'number' then
			value.burner.burnt_inventory_size = slot_mult(value.burner.burnt_inventory_size)
		end
	end

	::CONTINUE::
end

for index, value in pairs(data.raw['cargo-wagon']) do
	-- пропускаем прототипы мода cargo ships
	if cargo_ships[index] then goto CONTINUE end

	value.weight = value.weight * settings.startup['flattening-trains-weight-multiplier'].value

	local force, force_w = convert(value.braking_force)

	if force_w then
		value.braking_force = (force * settings.startup['flattening-trains-braking-force'].value) .. 'W'
	else
		value.braking_force = force * settings.startup['flattening-trains-braking-force'].value
	end

	value.max_health = value.max_health * settings.startup['flattening-trains-cargo-health'].value
	value.max_speed = value.max_speed * settings.startup['flattening-trains-max-speed'].value

	::CONTINUE::
end

for index, value in pairs(data.raw['fluid-wagon']) do
	-- пропускаем прототипы мода cargo ships
	if cargo_ships[index] then goto CONTINUE end

	value.weight = value.weight * settings.startup['flattening-trains-weight-multiplier'].value

	local force, force_w = convert(value.braking_force)

	if force_w then
		value.braking_force = (force * settings.startup['flattening-trains-braking-force'].value) .. 'W'
	else
		value.braking_force = force * settings.startup['flattening-trains-braking-force'].value
	end

	value.max_health = value.max_health * settings.startup['flattening-trains-fluid-cargo-health'].value
	value.max_speed = value.max_speed * settings.startup['flattening-trains-max-speed'].value

	::CONTINUE::
end

for index, value in pairs(data.raw['artillery-wagon']) do
	-- пропускаем прототипы мода cargo ships
	if cargo_ships[index] then goto CONTINUE end

	value.weight = value.weight * settings.startup['flattening-trains-weight-multiplier'].value

	local force, force_w = convert(value.braking_force)

	if force_w then
		value.braking_force = (force * settings.startup['flattening-trains-braking-force'].value) .. 'W'
	else
		value.braking_force = force * settings.startup['flattening-trains-braking-force'].value
	end

	value.max_health = value.max_health * settings.startup['flattening-trains-artillery-health'].value
	value.max_speed = value.max_speed * settings.startup['flattening-trains-max-speed'].value

	::CONTINUE::
end
