
data:extend({
	{
		type = 'double-setting',
		name = 'flattening-trains-weight-multiplier',
		setting_type = 'startup',
		default_value = 4,
		minimum_value = 0.01,
		order = 'a',
	},

	{
		type = 'double-setting',
		name = 'flattening-trains-power-multiplier',
		setting_type = 'startup',
		default_value = 4,
		minimum_value = 0.01,
		order = 'b',
	},

	{
		type = 'double-setting',
		name = 'flattening-trains-power-efficiency-multiplier',
		localized_description = {'mod-setting-description.flattening-trains-power-efficiency-multiplier'},
		setting_type = 'startup',
		default_value = 1,
		minimum_value = 0.01,
		order = 'b1',
	},

	{
		type = 'double-setting',
		name = 'flattening-trains-braking-force',
		setting_type = 'startup',
		default_value = 4,
		minimum_value = 0.01,
		order = 'c',
	},

	{
		type = 'double-setting',
		name = 'flattening-trains-loco-health',
		setting_type = 'startup',
		default_value = 2,
		minimum_value = 0.01,
		order = 'd1',
	},

	{
		type = 'double-setting',
		name = 'flattening-trains-cargo-health',
		setting_type = 'startup',
		default_value = 2,
		minimum_value = 0.01,
		order = 'd2',
	},

	{
		type = 'double-setting',
		name = 'flattening-trains-fluid-cargo-health',
		setting_type = 'startup',
		default_value = 2,
		minimum_value = 0.01,
		order = 'd3',
	},

	{
		type = 'double-setting',
		name = 'flattening-trains-artillery-health',
		setting_type = 'startup',
		default_value = 2,
		minimum_value = 0.01,
		order = 'd4',
	},

	{
		type = 'double-setting',
		name = 'flattening-trains-max-speed',
		setting_type = 'startup',
		default_value = 1,
		minimum_value = 0.01,
		order = 'e',
	},

	{
		type = 'double-setting',
		name = 'flattening-trains-fuel-slot-amount-multiplier',
		localized_description = {'mod-setting-description.flattening-trains-fuel-slot-amount-multiplier'},
		setting_type = 'startup',
		default_value = 1,
		minimum_value = 0.01,
		order = 'f1',
	},

	{
		type = 'int-setting',
		name = 'flattening-trains-fuel-slot-amount-flat',
		localized_description = {'mod-setting-description.flattening-trains-fuel-slot-amount-flat'},
		setting_type = 'startup',
		default_value = 0,
		order = 'f2',
	},
})
